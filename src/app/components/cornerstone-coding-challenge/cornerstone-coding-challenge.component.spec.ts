import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CornerstoneCodingChallengeComponent } from './cornerstone-coding-challenge.component';

describe('CornerstoneCodingChallengeComponent', () => {
  let component: CornerstoneCodingChallengeComponent;
  let fixture: ComponentFixture<CornerstoneCodingChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CornerstoneCodingChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CornerstoneCodingChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
