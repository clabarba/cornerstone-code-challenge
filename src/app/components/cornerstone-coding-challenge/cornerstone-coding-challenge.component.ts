import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-cornerstone-coding-challenge',
  templateUrl: './cornerstone-coding-challenge.component.html',
  styleUrls: ['./cornerstone-coding-challenge.component.css']
})
export class CornerstoneCodingChallengeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function(){

      console.log('js is working');

      $('div.hidden').fadeIn(1000).removeClass('hidden');
      
      $(window).scroll(function(){
        $(".search-container").css("opacity", 1 - $(window).scrollTop() / 150);
        $(".testimonials-container").css("opacity", 1 - $(window).scrollTop() / 1000);
      });
    });
  }

}
