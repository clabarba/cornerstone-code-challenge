import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

import { MatToolbarModule, MatFormFieldModule, MatInputModule, MatAutocompleteModule } from '@angular/material';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CornerstoneCodingChallengeComponent } from './components/cornerstone-coding-challenge/cornerstone-coding-challenge.component';

import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';


const routes: Routes = [
  { path: 'ccc', component: CornerstoneCodingChallengeComponent},
  { path: '', redirectTo: 'ccc', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    CornerstoneCodingChallengeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
