$(document).ready(function(){

    console.log('default JS file is working');

    $('div.hidden').fadeIn(1000).removeClass('hidden');
    
    $(window).scroll(function(){
      $(".search-container").css("opacity", 1 - $(window).scrollTop() / 150);
      $(".testimonials-container").css("opacity", 1 - $(window).scrollTop() / 1000);
    });
  });

new Awesomplete('input[data-multiple]', {
    filter: function(text, input) {
      return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
    },
  
    item: function(text, input) {
      return Awesomplete.ITEM(text, input.match(/[^,]*$/)[0]);
    },
  
    replace: function(text) {
      var before = this.input.value.match(/^.+,\s*|/)[0];
      this.input.value = before + text + ", ";
    }
  });

